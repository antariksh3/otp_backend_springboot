package otp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import otp.model.User;

@Service
public class VerifyService {

	public List<String> getAllTopics() {
		// TODO Auto-generated method stub
		return new ArrayList<>((Arrays.asList("Alex", "Brian", "Charles")));
	}
	
	public Response addUserToFirebase(User user, FirebaseDatabase firebaseDatabaseReference) {
		try {
			long currentTime = System.currentTimeMillis();
            DatabaseReference ref = firebaseDatabaseReference.getReference("Users");
            DatabaseReference ref2 = ref.push();
            ref2.child("name").setValue(user.getName());
            ref2.child("email").setValue(user.getEmail());
            System.out.println("User Saved - "+user.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return Response.ok("Access-Control-Allow-Origin", "application/json").build();
	}
}
